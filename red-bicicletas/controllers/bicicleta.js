var Bicicleta = require('../models/bicicleta');

exports.bicicleta_list = function(req, res) {
    Bicicleta.allBicis((err, bicicletas) => {
        res.render('bicicletas/index', { bicis: bicicletas });
    });
}

exports.bicicleta_create_get = function(req, res) {
    res.render('bicicletas/create');
}

exports.bicicleta_create_post = function(req, res) {

    console.log(req.body);
    var newBici = {
        "code": req.body.code,
        "color": req.body.color,
        "modelo": req.body.modelo,
        "ubicacion": [req.body.lat, req.body.lon]
    };

    console.log(newBici);

    Bicicleta.add(newBici, function(err, bici) {
        if (err) return console.log(err);
        res.redirect('/bicicletas');
    })
}

exports.bicicleta_update_get = function(req, res) {

    var bici = Bicicleta.findByCode(req.params.id, function(err, bici) {
        res.render('bicicletas/update', { bici });
    });
}

exports.bicicleta_update_post = function(req, res) {
    var query = { 'code': req.body.code };
    console.log('estoy en el update controller' + req.body.code);


    var biciUpdate = {
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lon]
    };

    Bicicleta.updateOne(query, biciUpdate, { upsert: true }, function(err, doc) {
        if (err) {
            console.log("Something wrong when updating data!");
        }
        res.redirect('/bicicletas');
    });
}

exports.bicicleta_delete_post = function(req, res) {

    console.log('estoy en el delete controller' + req.body.code);
    var query = { 'code': req.body.code };
    Bicicleta.deleteOne(query, function(err, doc) {
        if (err) {
            console.log("Something wrong when updating data!");
        }
        res.redirect('/bicicletas');
    });
}