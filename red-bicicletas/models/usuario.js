var mongoose = require('mongoose');
var Reserva = require('./reserva');

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;
const mailer = require('../mailer/mailer');
const mongooseUniqueValidator = require('mongoose-unique-validator');
const { callbackPromise } = require('nodemailer/lib/shared');

var Schema = mongoose.Schema;
const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'El nombre es requerido']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'Email obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Ingrese un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'Password obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});
usuarioSchema.plugin(mongooseUniqueValidator, { message: "El {PATH} ya está registrado" });

usuarioSchema.pre('save', function(next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds)
    }
    next();
});

usuarioSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.resetPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
    var reserva = new Reserva({
        usuario: this._id,
        bicicleta: biciId,
        desde: desde,
        hasta: hasta
    });
    console.log(reserva);
    reserva.save(cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({ _userId: this.id, token: crypto.randomBytes(16).toString('hex') });
    const email_destination = this.email;
    console.log('voy a enviar correo ');
    token.save(function(err) {
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificación de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:5000' + '\/token/confirmation\/' + Token.token + '.\n'
        };

        mailer.sendMail(mailOptions, function(err) {
            if (err) { return console.log(err.message); }
            console.log('se envio correo de verificación');
        });
    });
};

usuarioSchema.statics.findOneorCreateByGoogle = function findOneorCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            { 'googleId': condition.id }, { 'email': condition.emails[0].value }
        ]
    }, (err, result) => {
        if (result) {
            callback(err, result)
        } else {
            console.log('------------ condition --------');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = 'oauth';
            console.log('------------ values --------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err); }
                return callback(err + 'kgc', result);
            })
        }
    });
};

usuarioSchema.statics.findOneorCreateByFacebook = function findOneorCreate(condition, callback) {
    const self = this;
    console.log(condition);
    self.findOne({
        $or: [
            { 'facebookId': condition.id }, { 'email': condition.emails[0].value }
        ]
    }, (err, result) => {
        if (result) {
            callback(err, result)
        } else {
            console.log('------------ condition facebook -----------------');
            console.log(condition);
            let values = {};
            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');
            console.log('------------ values facebook -------------------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err); }
                return callback(err + 'kgc', result);
            })
        }
    });
};

module.exports = mongoose.model('Usuario', usuarioSchema);