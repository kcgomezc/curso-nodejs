const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const Usuario = require('../models/usuario');
var GoogleStrategy = require('passport-google-oauth2').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');

passport.use(new FacebookTokenStrategy({
    clientID: process.env.FACEBOOK_ID,
    clientSecret: process.env.FACEBOOK_SECRET
}, function(accessToken, refreshToken, profile, done) {
    try {
        Usuario.findOneorCreateByFacebook(profile, function(err, user) {
            if (err) console.log('err' + err);
            return done(err, user);
        });
    } catch (err2) {
        console.log('err2' + err2);
        return done(err2, null);
    }
}));

passport.use(new LocalStrategy(
    function(email, password, done) {
        Usuario.findOne({ email: email }, function(err, usuario) {
            if (err) { return done(err); }
            if (!usuario) {
                return done(null, false, { message: 'Email no existe.' });
            }
            if (!usuario.validPassword(password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }
            return done(null, usuario);
        });
    }
));

passport.use(new GoogleStrategy({
        clientID: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        callbackURL: 'http://red-bicicletas-kgc.herokuapp.com/auth/google/callback'
    },
    function(accessToken, refreshToken, profile, cb) {
        // User.findOrCreate({ googleId: profile.id }, function(err, user) {
        //     return done(err, user);
        // });
        Usuario.findOneorCreateByGoogle(profile, function(err, user) {
            return cb(err, user);
        })
    }
));

passport.serializeUser(function(user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
    Usuario.findById(id, function(err, usuario) {
        cb(err, usuario);
    });
});

module.exports = passport;